# Train App

Petite application pour m'amuser a avoir un affichage a la maison type affichage en garde pour les prochains départs

## Configuration

Certaines variables d'environnement sont nécessaires pour le bon fonctionnement de l'application : 

- `SNCF_API_KEY` : une clef d'API pour l'[API Digital SNCF](https://www.digital.sncf.com/startup/api)
- `REFRESH_INTERVAL` : le temps entre deux mises à jour, en secondes (default: `300`)
- `STATION`: le code de la gare pour laquelle vous voulez afficher les départs
- `DISPLAY_MODE`: par quel biais veut-on afficher les résultats (default: `console`, possibles: `lcd`, `dot-matrix`, `html`, `console`)

## Utilisation

- `yarn start:dev` pour le developpement local
- `yarn start` pour le deploiement en prod