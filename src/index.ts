import { SNCF } from "./infra/sncf/api";
import { Departure } from "./domain/departure";
import { Affichage, toAffichage } from "./domain/affichage";
import { DisplayFactory } from "./infra/display/display_factory";
import { Displayer } from "./infra/display/displayer";

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const CINQ_MINUTES = 5 * 60
const DISPLAY_MODE: string = (process.env.DISPLAY_MODE) ?? 'console'
const REFRESH_INTERVAL: number = (process.env.REFRESH_INTERVAL) ? parseInt(process.env.REFRESH_INTERVAL) : CINQ_MINUTES
const STATION: string = (process.env.STATION) ?? ''


const sncfApi: SNCF = new SNCF(process.env.SNCF_API_KEY)
const displayer: Displayer = DisplayFactory.create(DISPLAY_MODE)

async function main(): Promise<void> {
    if (!STATION) {
        throw new Error('NO_STATION_ID')
    }
    const departs: Departure[] = await sncfApi.getDeparturesFor(STATION)
    const affichages: Affichage[] = departs.map(toAffichage)
    const affichagesOrdonnes = affichages.sort((a, b) => {
        return (a.departEstime > b.departEstime) ? 1 : -1
    })

    displayer.output(affichagesOrdonnes)
}

console.log('Starting dashboard')
main()
setInterval(main, REFRESH_INTERVAL * 1000)

process.on('SIGINT', _ => {
    console.log('Exiting process')
    process.exit()
});