export interface Departure {
    stop_date_time: Horaires,
    display_informations: Display
}

export interface Horaires {
    arrival_date_time: string,
    departure_date_time: string,
    base_arrival_date_time: string,
    base_departure_date_time: string,
}

export interface Display {
    direction: string,
    code: string,
    network: string,
    color: string,
    name: string,
    physical_mode: string,
    headsign: string,
    text_color: string,
    commercial_mode: string,
    trip_short_name: string,
    links: Link[]
}

export interface Link {
    internal: boolean,
    type: string,
    id: string,
    rel: string,
    templated: boolean
}