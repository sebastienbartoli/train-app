import { DateTime } from "luxon";
import { Departure } from "./departure";

export interface Affichage {
    departEstime: string,
    destination: string,
    type: string,
    numero: string,
    ligne?: string,
    departReel?: string,
    couleurLigne?: string
    couleurFond?: string
    disruptions?: object[]
}

const TYPES_AVEC_LIGNES = [
    'TRANSILIEN',
    'RER'
]

export function toAffichage(departure: Departure) {
    const affichage: Affichage = {
        departEstime: DateTime.fromISO(departure.stop_date_time.base_departure_date_time).toISO(),
        destination: departure.display_informations.direction,
        type: departure.display_informations.network,
        numero: determineNumero(departure)
    }
    if (TYPES_AVEC_LIGNES.includes(affichage.type)) {
        affichage.ligne = departure.display_informations.code
        affichage.couleurLigne = departure.display_informations.color
    }
    if (departure.stop_date_time.departure_date_time != departure.stop_date_time.base_departure_date_time) {
        affichage.departReel = DateTime.fromISO(departure.stop_date_time.departure_date_time).toISO()
    }
    if (departure.display_informations.links) {
        if (departure.display_informations.links.some((link) => link.type === 'disruption')) {
            affichage.disruptions = departure.display_informations.links.filter((link) => link.type === 'disruption')
        }
    }

    return affichage
}

function determineNumero(departure: Departure) {
    switch (departure.display_informations.network) {
        case 'TRANSILIEN':
        case 'RER':
            return departure.display_informations.headsign
        default:
            return departure.display_informations.trip_short_name
    }
}