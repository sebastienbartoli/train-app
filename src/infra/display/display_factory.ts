import { ConsoleDisplay } from "./console.display"
import { Displayer } from "./displayer"

export class DisplayFactory {
    static create(mode: string): Displayer {
        switch (mode) {
            case 'lcd':
                throw new Error('DISPLAY_MODE_NOT_IMPLEMENTED')
                break
            case 'dot-matrix':
                throw new Error('DISPLAY_MODE_NOT_IMPLEMENTED')
                break
            case 'html':
                throw new Error('DISPLAY_MODE_NOT_IMPLEMENTED')
                break
            case 'console':
            default:
                return new ConsoleDisplay
        }
    }
}