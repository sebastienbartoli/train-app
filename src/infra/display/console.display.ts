import { DateTime } from "luxon";
import { Affichage } from "../../domain/affichage";
import { Displayer } from "./displayer";

export class ConsoleDisplay extends Displayer {
    output(affichages: Affichage[]): void {
        console.log(DateTime.now().toISO())
        console.log(affichages)
    }
}