import { Affichage } from "../../domain/affichage";

export abstract class Displayer {
    abstract output(affichages: Affichage[]): any;
}