import { Departure } from "../../domain/departure"
import got from "got"

interface DeparturesRessource {
    departures: Departure[],
    pagination: object,
    links: object,
    disruptions: object
}

export class SNCF {
    API_KEY: string

    constructor(api_key: string = process.env.SNCF_API_KEY || '') {
        this.API_KEY = api_key
        if (this.API_KEY === '') {
            throw new Error('NO_API_KEY_PROVIDED')
        }
    }

    async getDeparturesFor(station: string): Promise<Departure[]> {
        const response: DeparturesRessource = await got.get(`https://api.sncf.com/v1/coverage/sncf/stop_areas/${station}/departures`, {
            headers: {
                'Authorization': this.API_KEY
            }
        }).json()
        return response.departures
    }


}